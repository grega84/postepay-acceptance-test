Feature: Assistenza

@jiraid=PPPAY-592-555
  Scenario: PPPAY-536 - Assistenza - Verifica il corretto funzionamento della chat
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
     When the user tap on assistenza icon
      And procede to open chat
      | number     | 
      | 3333333333 | 
  
