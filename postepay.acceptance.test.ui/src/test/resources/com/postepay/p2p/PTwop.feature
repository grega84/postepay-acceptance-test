@P2P
Feature: P2P

 @jiraid=PPPAY-13 @P2POperation
  Scenario: PPPAY-13 - Money transfer with P2P function sent to contact saved in the contact list with name
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
       | userName               | password    | 
       | daniele.ioviero        | Prisma2019! | 
#      | gennaro.rega-1784      | Captur04$   | 
##     And "accept" the customization screen to access to homepage
     When the user tap on the P2P operation
      And complete the p2p operetion with following data
      | amount | recipient      | description     | posteId | 
      | 0.01   | 1Lu   | Test Automation | prisma  | 
     Then the user is redirected to the homepage and check the last movement
     | operationName | operationDate | amount | 
      | RICARICA POSTEPAY   |  2019   | - 0,01 | 


# @jiraid=PPPAY-13 @P2POperation
#  Scenario: PPPAY-131 - Money transfer with P2P function sent to contact saved in the contact list with number
  
#    Given open the PostepayApp and skip the training
#      And log in app with the credentials user with card already onborded
#     | userName               | password  | 
#      | daniele.ioviero     | Prisma2019! | 
#     And "accept" the customization screen to access to homepage
#     When the user tap on the P2P operation
#      And complete the p2p operetion with following data
#      | amount | recipient      | description     | posteId | 
#      | 0.01   | 3488377592  | Test Automation | prisma  | 
#     Then the user is redirected to the homepage and check the last movement
#      | operationName | operationDate | amount | 
#      | RICARICA POSTEPAY   |  2019   | - 0,01 | 
      
      
 #  @jiraid=PPPAY-708 @P2POperation
 #   Scenario: P2P - Verify the the operation P2P is correctly sent for only one contact
  
 #     Given open the PostepayApp and skip the training
 #       And log in app with the credentials user with card already onborded
 #        | userName               | password  | 
 #       | daniele.ioviero     | Prisma2019! | 
 #      And the user tap on the P2P operation
 #      And inser the data to search contact
 #       | amount | recipient      | 
 #      | 0.01   | 1Lu | 
 #      	And try to select the followings contact
 #       | amount | recipient      | 
 #       | 0.01   | 1Lntonio   | 
 #       When try to chenge the Contact
 #       | amount | recipient      | 
 #       | 0.01   | 1Luriana Salvino |
 #      Then check the contact selected
 #      | amount | recipient      | 
 #    	| 0.01   | 1Luriana Salvino   |
     	
     	
     	
 # @jiraid=PPPAY-1071
 # 	Scenario: check message for not valid amount 0.00�
 #       Given open the PostepayApp and skip the training
 #        And log in app with the credentials user with card already onborded
 #       | userName               | password  | 
 #       | daniele.ioviero     | Prisma2019! | 
 #     When the user tap on the P2P operation
 #      And insert the following amount and try to send operation
 #     | amount |
 #       | 0      | 
 #      Then the pop-up is shown with hte following message
 #      	|description													  | 
 #      	|Non puoi inserire importi pari a 0,00 �|
 
 
 
  # @jiraid=PPPAY-13 @P2POperation
  # Scenario: PPPAY-152 - Money request with P2P function request to contact saved in the contact list
  
   #  Given open the PostepayApp and skip the training
   #    And log in app with the credentials user with card already onborded
   #    | userName               | password  | 
    #   | daniele.ioviero     | Prisma2019! | 
 #     And "accept" the customization screen to access to homepage
   #   When the user tap on the P2P operation
  #     And complete the p2p operetion with following data
  #     | amount | requested      | description     | posteId | 
  #     | 0.01   | 1Luriana Salvino   | Test Automation | prisma  | 
   #   Then the user is redirected to the homepage and check the last movement
   #    | operationName | operationDate | amount | 
   #    | RICARICA POSTEPAY   |  2019   | - 0,01 | 
   
   
   
@jiraid=PPPAY-863 @P2POperation
  Scenario: PPPAY - 863 - Invio denaro dopo slide della rubrica
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
      And the user tap on the P2P operation
      And  inser the data to search contact
      | amount | recipient | 
      | 0.01   | 1Lu | 
      And try to select the followings contact
      | amount | recipient | 
      | 0.01   | 1Lu | 
      And check the contact selected
      | amount | recipient | 
      | 0.01   | 1Lu | 
     When try to modify the Contact
      | amount | recipient | description     | posteId | 
      | 0.01   | 122       | Test Automation | prisma  | 
     Then the user is redirected to the homepage and check the last movement
      | operationName     | operationDate | amount | 
      | RICARICA POSTEPAY | 2019          | - 0,01 | 
  

@jiraid=PPPAY-149 @P2POperation
  Scenario: PPPAY-149 - Money transfer with P2P function sent to contact not saved in the contact list_ANDROID_S8PLUS
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      | userName        | password    | posteId| 
      | daniele.ioviero | Prisma2019! | prisma| 
#        | userName               | password    | 
#        | gennaro.rega-1784      | Captur04$   | 
  ##     And "accept" the customization screen to access to homepage
     When the user tap on the P2P operation
      And complete the p2p operetion with following data for contact not saved
      | amount | recipient  | description     | posteId | 
      | 0.01   | 3472435838 | Test Automation | prisma  | 
     Then the user is redirected to the homepage and check the last movement
      | operationName     | operationDate | amount | 
      | RICARICA POSTEPAY | 2019          | - 0,01 |   
 
     	