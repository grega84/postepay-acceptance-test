Feature: G2G

@jiraid=PPPAY-592
  Scenario: PPPAY-592 - G2G - Invio giga
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
     | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
    When the user tap on the g2g button
      And procede to operation
     And search contact ad select it in the contact list
      | recipient          | posteId | 
      | Vincenzo Fortunato | prisma  | 
      And complete the g2g operation with posteid
      | posteId | 
      | prisma  | 
     Then the user receive the operation successfull screen and confim
     
@jiraid=PPPAY-294
  Scenario: PPPAY-294 - G2G - PP Connect - Acquista GB
  
   Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
     When the user tap on acquista giga button
      And complete the g2g operation with posteid
      | posteId | 
      | prisma  | 
 Then the user receives the success screen and confirms the purchase of GB
     
@jiraid=PPPAY-672
  Scenario: PPPAY-672 - G2G - Verifica che i numeri salvati con prefisso "+39" - "00"  vengano visualizzati come contatti connect
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded     
      | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
     When the user tap on the g2g button
      And procede to operation
      And search contact with 0039 ad select it in the contact list
      | recipient               | 
      | Vincenzo Fortunato 0039 | 
      And search contact with 39 ad select it in the contact list
      | recipient              | 
      | Vincenzo Fortunato +39 | 

@jiraid=PPPAY-574
  Scenario: PPPAY-911 - G2G - Verifica la modifica di un numero inserito manualmente
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
     When the user tap on the g2g button to send gb
      And procede to operation 
      And search an enabled contact ad select it in the contact list
      | recipient          |
      | Vincenzo Fortunato |
      And tap on icon to modify contact
      | recipient |  
      | 122 |   
    

      
@jiraid=PPPAY-593
  Scenario: PPPAY-593 - G2G - Verifica che i contatti Connect siano visualizzati nella rubrica "Contatti Connect"
  
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded     
      | userName        | password    | 
      | daniele.ioviero | Prisma2019! | 
      When the user tap on the g2g button
      And procede to operation to see the details