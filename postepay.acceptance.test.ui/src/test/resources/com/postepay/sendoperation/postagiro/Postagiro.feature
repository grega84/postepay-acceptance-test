@Postagiro
Feature: Postagiro

 @jiraid=PPPAY-941
 Scenario: Perform a Postagiro from card to conto
     Given open the PostepayApp and skip the training
       And log in app with the credentials user with card already onborded
       | userName               | password    | 
       | daniele.ioviero        | Prisma2019! | 
#        | userName               | password    | 
#        | gennaro.rega-1784      | Captur04$   | 
       When the user tap on the send and perform postagiro
      		|iban|description|destinationName|amount|posteId|
       		|IT23V0760103400001046214688|test|Daniele Ioviero |0,01|prisma|
      	Then the user receive the operation successfull screen and confim
        And the user is redirected to the homepage and check the last movement
 					|operationName|operationDate|amount|
 					|COMMISSIONE POSTAGIRO|  2019|- 0,50|
  				|POSTAGIRO|  2019|- 0,01|
				
@jiraid=PPPAY-940
  Scenario: Perform a Postagiro from card to card
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
       | userName               | password    | 
       | daniele.ioviero        | Prisma2019! | 
#       | userName               | password    | 
#       | gennaro.rega-1784      | Captur04$   | 
      When the user tap on the send and perform postagiro
      		|iban|description|destinationName|amount|posteId|
      		|IT13P3608105138280234980240|test|Luisa Barbarino |0,01|prisma|
     	Then the user receive the operation successfull screen and confim
      And the user is redirected to the homepage and check the last movement
					|operationName|operationDate|amount|
					|COMMISSIONE POSTAGIRO|  2019|- 0,50|
					|POSTAGIRO|  2019|- 0,01|
					
					
#@jiraid=PPPAY-467
#  Scenario: Perform a Postagiro during cut-off time 
#    Given open the PostepayApp and skip the training
#      And log in app with the credentials user with card already onborded
#      | userName               | password  | 
#      | daniele.ioviero     | Prisma2019! |
#      And "accept" the customization screen to access to homepage
#      When the user tap on the send and perform postagiro
#      		|iban|description|destinationName|amount|posteId|
#      		|IT23V0760103400001046214688|test|Daniele Ioviero|0,01|prisma|
#     	Then the system response with page transaction in charge
#      And the user is redirected to the homepage and check the last movement
#				|operationName|operationDate|amount|
#					|COMMISSIONE POSTAGIRO|  2019|- 0,50|
#					|POSTAGIRO|  2019|- 0,01|
					