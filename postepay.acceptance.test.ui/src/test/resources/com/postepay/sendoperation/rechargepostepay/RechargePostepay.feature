@RechargePostepay
Feature: Recharge Postepay

@jiraid=PPPAY-6
  Scenario: Recharge other PostePay
 
    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
       | userName               | password    | 
       | daniele.ioviero        | Prisma2019! | 
 #      | userName               | password    | 
 #      | gennaro.rega-1784      | Captur04$   | 
#      And "accept" the customization screen to access to homepage
      When the user tap on the send and recharge other postepay
      		|cardNumber|destinationName|amount|posteId|
      		|4023600959866981|Luisa Barbarino|0,01	|prisma		|
     	Then the user receive the operation successfull screen and confim
      And the user is redirected to the homepage and check the last movement
					|operationName|operationDate|amount|
					|COMMISSIONE RICARICA POSTEPAY| 2019|- 1,00|
					|RICARICA POSTEPAY| 2019|- 0,01|
					
					
#@jiraid=PPPAY-797
#  Scenario: Recharge PostePay- verify message for transaction error in wrong time
 
#    Given open the PostepayApp and skip the training
#      And log in app with the credentials user with card already onborded
#      |userName|password|
#      |daniele.ioviero|Prisma2019!|
#      And "accept" the customization screen to access to homepage
#      When the user tap on the send and recharge other postepay
#      		|cardNumber|destinationName|amount|posteId|
#         |4023600959866981|Luisa Barbarino|0,01	|555555		|
#     	Then the system response with page error transaction
#					|transactionMessage|operationText|
#					|Transazione non riuscita|Il servizio e' disponibile solo all'interno di una fascia oraria prestabilita.|

