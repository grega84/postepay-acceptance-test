@RechargePostepay
Feature: Bonifico

 @jiraid=PPPAY-943
   Scenario: Bonifico Operation NO POSTE

     Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
       | userName               | password    | 
       | daniele.ioviero        | Prisma2019! | 
 #      | userName               | password    | 
 #      | gennaro.rega-1784      | Captur04$   | 
     # And "accept" the customization screen to access to homepage
      When the user tap on the send and perform Bonifico
       		|iban|destinationName|amount|posteId|
       		|IT39U3287501600N14674160015|Daniele Ioviero|0,01|prisma|
       Then the user receive the operation successfull screen and confim
       And the user is redirected to the homepage and check the last movement
	 				|operationName|operationDate|amount|
	 				| COMMISSIONE BONIFICO| 2019|- 1,00|
	 				| BONIFICO| 2019|- 0,01|
					
#@jiraid=PPPAY-466
#  Scenario: Bonifico Operation- during CUT-OFF time NO POSTE
#					
#   Given open the PostepayApp and skip the training
#      And log in app with the credentials user with card already onborded
#       | userName               | password  | 
#       | daniele.ioviero     | Prisma2019! | 
#     And "accept" the customization screen to access to homepage
#      When the user tap on the send and perform Bonifico
#      		|iban|destinationName|amount|posteId|
#      		|IT51T0306903491100000001379|Gennaro Rega|0,01|prisma|
#    	Then the system response with page transaction in charge
#     And the user is redirected to the homepage and check the last movement
#					|operationName|operationDate|amount|
#					| PRESA IN CARICO| 2019|- 0,01|

 @jiraid=PPPAY-942
 Scenario: Bonifico Operation POSTE

     Given open the PostepayApp and skip the training
       And log in app with the credentials user with card already onborded
       | userName               | password    | 
       | daniele.ioviero        | Prisma2019! | 
#      | userName               | password    | 
#      | gennaro.rega-1784      | Captur04$   | 
#      And "accept" the customization screen to access to homepage
       When the user tap on the send and perform Bonifico
       		|iban|destinationName|amount|posteId|
       		|IT23V0760103400001046214688|Daniele Ioviero|0,01|prisma|
      	Then the user receive the operation successfull screen and confim
       And the user is redirected to the homepage and check the last movement
 					|operationName|operationDate|amount|
 					| COMMISSIONE POSTAGIRO| 2019|- 0,50|
 					| POSTAGIRO | 2019|- 0,01|