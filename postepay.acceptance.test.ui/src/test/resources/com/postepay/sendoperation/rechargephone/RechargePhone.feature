@RechargePhone
Feature: Recharge Phone

@jiraid=PPPAY-6
  Scenario: Recharge phone

    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      |userName|password|
      |daniele.ioviero|Prisma2019!|
      And "accept" the customization screen to access to homepage
      When the user tap on the send and recharge phone
      		|phoneNumber|phoneOperator|amount|posteId|
      		|3357151799	|VODAFONE|10.00	|prisma		|
     	Then the user receive the operation successfull screen and confim
      And the user is redirected to the homepage and check the last movement
					|operationName|operationDate|amount|
					|P2P INVIATO 12/04/2019 16.40 Operazione da canale mobile|12 apr 2019|- 0,01 �|
					
@jiraid=PPPAY-462					
 Scenario: Recharge phone - verify error message in wrong time

    Given open the PostepayApp and skip the training
      And log in app with the credentials user with card already onborded
      |userName|password|
      |daniele.ioviero|Prisma2019!|
      And "accept" the customization screen to access to homepage
      When the user tap on the send and recharge phone
      		|phoneNumber|phoneOperator|amount|posteId|
      		|3357151799	|TIM|10.00	|prisma		|
     	Then the system response with page error transaction
					|transactionMessage|operationText|
					|Transazione non riuscita|Il servizio e' disponibile solo all'interno di una fascia oraria prestabilita.|
