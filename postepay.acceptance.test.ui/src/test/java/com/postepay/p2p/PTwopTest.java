package com.postepay.p2p;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/P2p.json" },
features = {"src/test/resources/com/postepay/P2p/PTwop.feature"})
@Ui
public class PTwopTest {
}
