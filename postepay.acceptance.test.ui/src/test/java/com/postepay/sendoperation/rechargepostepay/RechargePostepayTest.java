package com.postepay.sendoperation.rechargepostepay;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/RechargePostepay.json" },
features = {"src/test/resources/com/postepay/sendoperation/rechargepostepay/RechargePostepay.feature"})
@Ui
public class RechargePostepayTest {

}
