package com.postepay.sendoperation.postagiro;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Postagiro.json" },
features = {"src/test/resources/com/postepay/sendoperation/postagiro/Postagiro.feature"})
@Ui
public class PostagiroTest {

}
