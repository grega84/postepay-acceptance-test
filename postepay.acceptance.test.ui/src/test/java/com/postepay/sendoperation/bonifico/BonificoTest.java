package com.postepay.sendoperation.bonifico;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Bonifico.json" },
features = {"src/test/resources/com/postepay/sendoperation/bonifico/Bonifico.feature"})
@Ui
public class BonificoTest {

}
