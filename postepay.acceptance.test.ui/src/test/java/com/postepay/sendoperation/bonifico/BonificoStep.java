package com.postepay.sendoperation.bonifico;


import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import bean.datatable.BonificoBean;
import bean.datatable.CheckMovementBean;
import bean.datatable.CredentialBean;
import bean.datatable.P2pBean;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import patternpage.CustomizationPage;
import patternpage.CustomizationPage.TypeButton;
import patternpage.HomePage;
import patternpage.LoginPage;
import patternpage.OperationCompletedPage;
import patternpage.P2PPage;
import patternpage.ThankYouPage;
import patternpage.TutorialP2PPage;
import patternpage.TutorialPage;
import test.automation.core.UIUtils;

public class BonificoStep {
	private AppiumDriver<MobileElement> driver=null;
	private TutorialPage tutorial;
	private LoginPage loginPage;
	private HomePage homepage;
	private TutorialP2PPage tutorialP2PPage;
	private CustomizationPage customizationPage;
	private P2PPage p2pPage;
	private OperationCompletedPage operationCompletedPage;
	
	@After
	public void teardown() {
		driver.quit();		
	}
	@Given("^open the PostepayApp and skip the training$")
	public void open_the_PostepayApp_and_skip_the_training() throws Throwable {
		   driver = (AppiumDriver<MobileElement>) UIUtils.ui().driver("mobile");
//	   tutorial= new TutorialPage(driver).get();
//	   loginPage=tutorial.goToLogin();
	    loginPage= new LoginPage(driver).get();
	    
	}

	@Given("^log in app with the credentials user with card already onborded$")
	public void log_in_app_with_the_credentials_user_with_card_already_onborded(List<CredentialBean> credentialsBean) throws Throwable {
//		customizationPage=loginPage.loginFromPosteItToCustomizationPage(credentialsBean.get(0));
		homepage=loginPage.loginFromPosteIt(credentialsBean.get(0));
		
		
	    
	}

	@Given("^\"([^\"]*)\" the customization screen to access to homepage$")
	public void the_customization_screen_to_access_to_homepage(String typeButton) throws Throwable {
		String valueTmp;
		if(TypeButton.ACCEPT.getNameButton().equals(typeButton)){
			homepage=customizationPage.goToHomepage(TypeButton.ACCEPT);	
		}else if(TypeButton.DENY.getNameButton().equals(typeButton)){
			homepage=customizationPage.goToHomepage(TypeButton.DENY);
		}else{
		    throw new IllegalArgumentException("The value "+ typeButton +" is not valid");
		}
	}

	@When("^the user tap on the P(\\d+)P operation$")
	public void the_user_tap_on_the_P_P_operation(int arg1) throws Throwable {
	    tutorialP2PPage=homepage.goToP2PConfiguration();
	    
	}

//	@When("^complete the p(\\d+)p operetion with following data$")
//	public void complete_the_p_p_operetion_with_following_data(int arg1, List<P2pBean> p2pBeanList) throws Throwable {
//		p2pPage=tutorialP2PPage.goP2pOperation();
//		homepage=p2pPage.completeP2pOperation(p2pBeanList.get(0));
//	}

	@Then("^the user is redirected to the homepage and check the last movement$")
	public void the_user_is_redirected_to_the_homepage_and_check_the_last_movement(List<CheckMovementBean> checkMovements) throws Throwable {
		homepage.checkLastMovement(checkMovements.get(0));
	
	}
	
//	
	@Then("^the user receive the operation successfull screen and confim$")
	public void the_user_receive_the_operation_successfull_screen_and_confim() throws Throwable {
		ThankYouPage tnkPage=new ThankYouPage(driver).get();
		tnkPage.closeThankYouPage();
		//homepage=operationCompletedPage.closePage();
		
	}
	@When("^the user tap on the send and perform Bonifico$")
	public void the_user_tap_on_the_send_and_perform_Bonifico(List<BonificoBean>  bonificoList) throws Throwable {
		operationCompletedPage= homepage.sendBonifico(bonificoList.get(0));
		
	
	}
	@After
	public void runHook(Scenario scenario){
		if (scenario.isFailed()){
			//takes a screenshot for failed scenario
			takeScreenshotOnFailure(scenario);
		}else{
			//embeds the screenshot taken if any
			embedScreenshot(scenario);
		}
		driver.quit();
	}
	
	private void takeScreenshotOnFailure(Scenario scenario){
		try {
			//WebDriver currentDriver = UIUtils.ui().getCurrentDriver();
			//String fileName = UIUtils.ui().takeScreenshot(currentDriver);
			//scenario.write("<img src=\"embeddings\\" + fileName + " \" >");
    	} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void embedScreenshot(Scenario scenario){
		int scenarioID = scenario.getId().hashCode();
		String reportImagesDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		try {
			List<Path> listImagesFile = Files.walk(Paths.get(reportImagesDir))
			 .filter(Files::isRegularFile)
			 .filter(f -> StringUtils.contains(f.getFileName().toString(), Integer.toString(scenarioID)))
			 .collect(toList());
			for (Path path : listImagesFile) {
				scenario.write("<img src=\"embeddings\\" + path.toFile().getName() + " \" >");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
