package com.postepay.bill;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/bill.json" },
features = {"src/test/resources/com/postepay/bill/Bill.feature"})
@Ui
public class BillTest {
}
