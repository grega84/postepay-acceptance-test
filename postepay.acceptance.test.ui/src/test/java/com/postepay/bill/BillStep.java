package com.postepay.bill;


import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import bean.datatable.CheckMovementBean;
import bean.datatable.CredentialBean;
import bean.datatable.P2pBean;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import patternpage.CustomizationPage;
import patternpage.CustomizationPage.TypeButton;
import patternpage.HomePage;
import patternpage.LoginPage;
import patternpage.P2PPage;
import patternpage.TutorialP2PPage;
import patternpage.TutorialPage;
import test.automation.core.UIUtils;

public class BillStep {
	private AppiumDriver<MobileElement> driver=null;
	private TutorialPage tutorial;
	private LoginPage loginPage;
	private HomePage homepage;
	private TutorialP2PPage tutorialP2PPage;
	private CustomizationPage customizationPage;
	private P2PPage p2pPage;
	@Before
	public void setup(Scenario scenario) throws IOException {
	System.out.println(scenario.getId());
	System.out.println(scenario.getName());
	System.setProperty(UIUtils.SCENARIO_ID, Integer.toString(scenario.getId().hashCode()));
	}
	@Given("^open the PostepayApp and skip the training$")
	public void open_the_PostepayApp_and_skip_the_training() throws Throwable {
		   driver = (AppiumDriver<MobileElement>) UIUtils.ui().driver("mobile");
//		   tutorial= new TutorialPage(driver).get();
//		   loginPage=tutorial.goToLogin();
		    loginPage= new LoginPage(driver).get();
		    UIUtils.ui().takeScreenshot((AndroidDriver)driver);
	    
	    
	}


	public void log_in_app_with_the_credentials_user_with_card_already_onborded(List<CredentialBean> credentialsBean) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
//		customizationPage=loginPage.loginFromPosteItToCustomizationPage(credentialsBean.get(0));
		homepage=loginPage.loginFromPosteIt(credentialsBean.get(0));
		
		
	    
	}

	@Given("^\"([^\"]*)\" the customization screen to access to homepage$")
	public void the_customization_screen_to_access_to_homepage(String typeButton) throws Throwable {
		String valueTmp;
		if(TypeButton.ACCEPT.getNameButton().equals(typeButton)){
			homepage=customizationPage.goToHomepage(TypeButton.ACCEPT);	
		}else if(TypeButton.DENY.getNameButton().equals(typeButton)){
			homepage=customizationPage.goToHomepage(TypeButton.DENY);
		}else{
		    throw new IllegalArgumentException("The value "+ typeButton +" is not valid");
		}
	}

	@When("^the user tap on the P(\\d+)P operation$")
	public void the_user_tap_on_the_P_P_operation(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		p2pPage=homepage.goToP2PPage();
	    
	}


	public void complete_the_p_p_operetion_with_following_data(int arg1, List<P2pBean> p2pBeanList) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
//		p2pPage=tutorialP2PPage.goP2pOperation();
		homepage=p2pPage.completeP2pOperation(p2pBeanList);
	}

	
	public void the_user_is_redirected_to_the_homepage_and_check_the_last_movement(List<CheckMovementBean> checkMovements) throws Throwable {
		homepage.checkLastMovement(checkMovements.get(0));
	
	}

	@After
	public void runHook(Scenario scenario){
		if (scenario.isFailed()){
			//takes a screenshot for failed scenario
			takeScreenshotOnFailure(scenario);
		}else{
			//embeds the screenshot taken if any
			embedScreenshot(scenario);
		}
	}
	
	private void takeScreenshotOnFailure(Scenario scenario){
		try {
			WebDriver currentDriver = UIUtils.ui().getCurrentDriver();
			String fileName = UIUtils.ui().takeScreenshot(currentDriver);
			scenario.write("<img src=\"embeddings\\" + fileName + " \" >");
    	} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void embedScreenshot(Scenario scenario){
		int scenarioID = scenario.getId().hashCode();
		String reportImagesDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		try {
			List<Path> listImagesFile = Files.walk(Paths.get(reportImagesDir))
			 .filter(Files::isRegularFile)
			 .filter(f -> StringUtils.contains(f.getFileName().toString(), Integer.toString(scenarioID)))
			 .collect(toList());
			for (Path path : listImagesFile) {
				scenario.write("<img src=\"embeddings\\" + path.toFile().getName() + " \" >");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
