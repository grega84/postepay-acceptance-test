package com.postepay.login;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Login.json" },
features = {"src/test/resources/com/postepay/login/Login.feature"})
@Ui
public class LoginTest {
}
