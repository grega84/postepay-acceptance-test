package com.postepay.assistenza;


import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import bean.datatable.AssistenzaBean;
import bean.datatable.CheckMovementBean;
import bean.datatable.CredentialBean;
import bean.datatable.P2pBean;
import bean.datatable.PostagiroBean;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import patternpage.AssistenzaPage;
import patternpage.CustomizationPage;
import patternpage.CustomizationPage.TypeButton;
import patternpage.HomePage;
import patternpage.LoginPage;
import patternpage.OperationCompletedPage;
import patternpage.P2PPage;
import patternpage.TutorialP2PPage;
import patternpage.TutorialPage;
import test.automation.core.UIUtils;

public class AssistenzaStep {
	private AppiumDriver<MobileElement> driver=null;
	private TutorialPage tutorial;
	private LoginPage loginPage;
	private HomePage homepage;
	private AssistenzaPage assistenzaPage;
	
	@Given("^open the PostepayApp and skip the training$")
	public void open_the_PostepayApp_and_skip_the_training() throws Throwable {
		   driver = (AppiumDriver<MobileElement>) UIUtils.ui().driver("mobile");
//		   tutorial= new TutorialPage(driver).get();
//		   loginPage=tutorial.goToLogin();
		    loginPage= new LoginPage(driver).get();
	}

	@And("^log in app with the credentials user with card already onborded$")
	public void log_in_app_with_the_credentials_user_with_card_already_onborded(List<CredentialBean> credentialsBean) throws Throwable {
		homepage=loginPage.loginFromPosteIt(credentialsBean.get(0));
	}

    @When("^the user tap on assistenza icon$")
    public void the_user_tap_on_assistenza_icon() throws Throwable {
    	assistenzaPage = new AssistenzaPage(driver).get();
    	assistenzaPage.openAssistenza();
    }
    
    @And("^procede to open chat$")
    public void procede_to_open_chat(List<AssistenzaBean> assistenzaBean) throws Throwable {
    	assistenzaPage.openChattaConNoiPage(assistenzaBean.get(0));
    }
	
	
    @Then("^boh$")
    public void boh() throws Throwable {
    	
    }
}
