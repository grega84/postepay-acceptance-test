package com.postepay.assistenza;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Assistenza.json" },
features = {"src/test/resources/com/postepay/assistenza/Assistenza.feature"})
@Ui
public class AssistenzaTest {

}
