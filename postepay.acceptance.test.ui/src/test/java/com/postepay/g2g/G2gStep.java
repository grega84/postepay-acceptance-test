package com.postepay.g2g;


import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;

import android.Locator;
import bean.datatable.CheckMovementBean;
import bean.datatable.CredentialBean;
import bean.datatable.G2gBean;
import bean.datatable.P2pBean;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import patternpage.CustomizationPage;
import patternpage.CustomizationPage.TypeButton;
import patternpage.G2GAcquistaPage;
import patternpage.G2GCompilePage;
import patternpage.G2GDetailsGbPage;
import patternpage.G2GPage;
import patternpage.G2GPosteIDPage;
import patternpage.G2GRegalaPage;
import patternpage.G2GRiepilogoPage;
import patternpage.HomePage;
import patternpage.LoginPage;
import patternpage.OperationCompletedPage;
import patternpage.P2PInviaPage;
import patternpage.P2PPage;
import patternpage.ThankYouPageG2G;
import patternpage.TutorialP2PPage;
import patternpage.TutorialPage;
import test.automation.core.TestDataUtils;
import test.automation.core.UIUtils;

public class G2gStep {
	private AppiumDriver<MobileElement> driver=null;
	private TutorialPage tutorial;
	private LoginPage loginPage;
	private HomePage homepage;
	//private TutorialG2GPage tutorialG2GPage;
	private CustomizationPage customizationPage;
	private G2GPage g2gPage;
	private G2GDetailsGbPage g2gDetailsGbPage;
	private G2GRegalaPage g2gRegalaPage;
	private G2GRegalaPage g2gPageCompile;
	private G2GCompilePage g2gCompilePage;
	private G2GRiepilogoPage g2gRiepologoPage;
	private G2GPosteIDPage g2gPosteIDPage;
	private ThankYouPageG2G thankYouPageG2G;
	private G2GAcquistaPage g2gAcquistaPage;
	private OperationCompletedPage operationCompletedPage;
	
	@Before
	public void setup(Scenario scenario) throws IOException {
	System.out.println(scenario.getId());
	System.out.println(scenario.getName());
	System.setProperty(UIUtils.SCENARIO_ID, Integer.toString(scenario.getId().hashCode()));
	}
	
	@Given("^open the PostepayApp and skip the training$")
	public void open_the_PostepayApp_and_skip_the_training() throws Throwable {
		   driver = (AppiumDriver<MobileElement>) UIUtils.ui().driver("mobile");
//		   tutorial= new TutorialPage(driver).get();
//		   loginPage=tutorial.goToLogin();
		    loginPage= new LoginPage(driver).get();
		    UIUtils.ui().takeScreenshot(driver);
	}

	@Given("^log in app with the credentials user with card already onborded$")
	public void log_in_app_with_the_credentials_user_with_card_already_onborded(List<CredentialBean> credentialsBean) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
//		customizationPage=loginPage.loginFromPosteItToCustomizationPage(credentialsBean.get(0));
		homepage=loginPage.loginFromPosteIt(credentialsBean.get(0));
	}

	@Given("^\"([^\"]*)\" the customization screen to access to homepage$")
	public void the_customization_screen_to_access_to_homepage(String typeButton) throws Throwable {
		String valueTmp;
		if(TypeButton.ACCEPT.getNameButton().equals(typeButton)){
			homepage=customizationPage.goToHomepage(TypeButton.ACCEPT);	
		}else if(TypeButton.DENY.getNameButton().equals(typeButton)){
			homepage=customizationPage.goToHomepage(TypeButton.DENY);
		}else{
		    throw new IllegalArgumentException("The value "+ typeButton +" is not valid");
		}
	}

    @When("^the user tap on the g2g button$")
    public void the_user_tap_on_the_g2g_button() throws Throwable {
    	g2gPage=homepage.goToG2GPage();
    }

    @And("^procede to operation$")
    public void procede_to_operation() throws Throwable {
    	g2gPage.goToG2GRegalaPage();
    	g2gRegalaPage=new G2GRegalaPage(driver);
    	g2gRegalaPage.goToG2GCompile();
    }

    @And("^search contact ad select it in the contact list$")
	public void inser_the_data_to_search_contact(List<G2gBean> g2gBeanList) throws Throwable { 
    	g2gCompilePage = new G2GCompilePage(driver);
		g2gCompilePage.searchContact(g2gBeanList.get(0));
		g2gCompilePage.selectContact(g2gBeanList.get(0));
		g2gCompilePage.goNext();
    	g2gRiepologoPage = new G2GRiepilogoPage(driver);
    	g2gRiepologoPage.checkContactDestination(g2gBeanList.get(0));
    	g2gRiepologoPage.goInvia();
	}
    
    
    @And("^search an enabled contact ad select it in the contact list$")
    public void search_an_enabled_contact_ad_select_it_in_the_contact_list(List<G2gBean> g2gBeanList) throws Throwable {
    	g2gCompilePage = new G2GCompilePage(driver);
		g2gCompilePage.searchContact(g2gBeanList.get(0));
		g2gCompilePage.selectContact(g2gBeanList.get(0));
		g2gCompilePage.goNext();
    	g2gRiepologoPage = new G2GRiepilogoPage(driver);
    	g2gRiepologoPage.checkContactDestination(g2gBeanList.get(0));
    }
    
    @And("^complete the g2g operation with posteid$")
    public void complete_the_g2g_operation_with_posteid(List<G2gBean> g2gBeanList) throws Throwable {
    	g2gPosteIDPage = new G2GPosteIDPage(driver);
    	g2gPosteIDPage.completeOperation(g2gBeanList.get(0));
    }

    @Then("^the user receive the operation successfull screen and confim$")
    public void the_user_receive_the_operation_successfull_screen_and_confim() throws Throwable {
    	thankYouPageG2G = new ThankYouPageG2G(driver).get();
    	thankYouPageG2G.closeThankYouPage();
    }
    
    @And("^search contact with 0039 ad select it in the contact list$")
    public void search_contact_with_0039_ad_select_it_in_the_contact_list(List<G2gBean> g2gBeanList) throws Throwable {
    	g2gCompilePage = new G2GCompilePage(driver);
		g2gCompilePage.searchContact(g2gBeanList.get(0));
		g2gCompilePage.checklogo(g2gBeanList.get(0));
    }

    @And("^search contact with 39 ad select it in the contact list$")
    public void search_contact_with_39_ad_select_it_in_the_contact_list(List<G2gBean> g2gBeanList) throws Throwable {
    	g2gCompilePage = new G2GCompilePage(driver);
		g2gCompilePage.searchContact(g2gBeanList.get(0));
		g2gCompilePage.checklogo(g2gBeanList.get(0));
    }
    
    @And("^procede to operation to see the details$")
    public void procede_to_operation_to_see_the_details() throws Throwable {
    	g2gPage = new G2GPage(driver);
    	g2gDetailsGbPage = new G2GDetailsGbPage(driver);
    	g2gPage.openSeeDetails();
    	g2gDetailsGbPage.checkElements();
    }
    
    @When("^the user tap on acquista giga button$")
    public void the_user_tap_on_acquista_giga_button() throws Throwable {
    	g2gPage=homepage.goToG2GPage();
    	g2gPage = new G2GPage(driver).get();
    	g2gPage.openAquistaPage();
    	g2gAcquistaPage = new G2GAcquistaPage(driver).get();
    	g2gAcquistaPage.checkElementsAquistaGiga();
    	g2gAcquistaPage.pagaAcquistaGiga();
    }
    
    @When("^the user tap on the g2g button to send gb$")
    public void the_user_tap_on_the_g2g_button_to_send_gb() throws Throwable {
    	g2gPage=homepage.goToG2GPage();
    }
    
    @And("^tap on icon to modify contact$")
    public void tap_on_icon_to_modify_contact(List<G2gBean> g2gBeanList) throws Throwable {
    	g2gRiepologoPage.modificaDestinatario();
    	g2gCompilePage.searchContact(g2gBeanList.get(0));
		g2gCompilePage.selectContact(g2gBeanList.get(0));
		g2gCompilePage.goNext();
    	g2gRiepologoPage = new G2GRiepilogoPage(driver);
    	g2gRiepologoPage.checkContactDestination(g2gBeanList.get(0));
    }
    
    @Then("^the user receives the success screen and confirms the purchase of GB$")
    public void the_user_receives_the_success_screen_and_confirms_the_purchase_of_gb() throws Throwable {
    	thankYouPageG2G = new ThankYouPageG2G(driver).get();
    	thankYouPageG2G.closeThankYouPage();
    }
    
    @After
	public void runHook(Scenario scenario){
		if (scenario.isFailed()){
			takeScreenshotOnFailure(scenario);
		}else{
			embedScreenshot(scenario);
		}
		driver.quit();
	}
	
	private void takeScreenshotOnFailure(Scenario scenario){
		try {
			WebDriver currentDriver = UIUtils.ui().getCurrentDriver();
			String fileName = UIUtils.ui().takeScreenshot(currentDriver);
			scenario.write("<img src=\"embeddings\\" + fileName + " \" >");
    	} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void embedScreenshot(Scenario scenario){
		int scenarioID = scenario.getId().hashCode();
		String reportImagesDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		try {
			List<Path> listImagesFile = Files.walk(Paths.get(reportImagesDir))
			 .filter(Files::isRegularFile)
			 .filter(f -> StringUtils.contains(f.getFileName().toString(), Integer.toString(scenarioID)))
			 .collect(toList());
			for (Path path : listImagesFile) {
				scenario.write("<img src=\"embeddings\\" + path.toFile().getName() + " \" >");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
 
}
