package com.postepay.g2g;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/G2g.json" },
features = {"src/test/resources/com/postepay/g2g/G2g.feature"})
@Ui
public class G2gTest {
}
